/**	@file	ipmc.c
 *	@brief	IP Multicast Test Program
 *
 *	@author	yuwan@innopiatech.com
 *	@date	2004. 12. 16.
 */

#define _IPMC_C

#include "ipmc.h"

/**	Global variables
 */
Role			g_eRole = ROLE_NONE;	//!< Sender or Receiver
struct in_addr		g_sMcastLocalIfAddr;	//!< Source Interface IP Addr
struct sockaddr_in	g_sMcastGroupAddr;	//!< Target Group IP Address
char			g_binData[256];		//!< Multicast data
unsigned char		g_nMcastTTL = 1;	//!< Multicast TTL value

char*			g_szOptions = "hSRi:g:p:d:t:b:";	//!< Options
struct option		g_arrLongOpts[] = {	//!< Long Options
	/* name,		has_arg,		flag,	val */
	{"help",		no_argument,		NULL,	'h'},
	{"sender",		no_argument,		NULL,	'S'},
	{"receiver",		no_argument,		NULL,	'R'},
	{"mcast-local-if-addr",	required_argument,	NULL,	'i'},
	{"mcast-group-addr",	required_argument,	NULL,	'g'},
	{"mcast-group-port",	required_argument,	NULL,	'p'},
	{"mcast-data",		required_argument,	NULL,	'd'},
	{"mcast-ttl",		required_argument,	NULL,	't'},
	{"mcast-loopback",	required_argument,	NULL,	'b'}
};

int			g_nSocket = 0;		//!< Socket Descriptor
MCAST_LOOPBACK		g_eLoopback = LOOPBACK_NONE; //!< Loopback?
char			g_szMcastLocalIfAddr[64]; //!< Local Interface IP
char			g_szMcastGroupAddr[64];  //!< Mcast Group IP
int			g_nMcastGroupPort;	//!< Mcast Group Port
struct ip_mreq		g_sMcastJoinReq;	//!< Multicast Join Request

/**	Init Global Variables
 */
void
init_global_variables(void)
{
	g_eRole = ROLE_NONE;

	/* Multicast Local Interface Address */
	memset(&g_sMcastLocalIfAddr, 0, sizeof(g_sMcastLocalIfAddr));
	g_sMcastLocalIfAddr.s_addr = htonl(INADDR_ANY);

	strcpy(g_szMcastLocalIfAddr, "*");

	/* Multicast Group Address & Port */
	memset(&g_sMcastGroupAddr, 0, sizeof(g_sMcastGroupAddr));
	g_sMcastGroupAddr.sin_family = AF_INET;
	g_sMcastGroupAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	g_sMcastGroupAddr.sin_port = htons(DEFAULT_UDP_TARGET_PORT);

	strcpy(g_szMcastGroupAddr, "*");
	g_nMcastGroupPort = DEFAULT_UDP_TARGET_PORT;

	/* Multicast Data */
	strcpy(g_binData, DEFAULT_MCAST_DATA);

	g_nMcastTTL = 1;

	g_eLoopback = LOOPBACK_NONE;

	/* Multicast Join Request */
	memset(&g_sMcastJoinReq, 0, sizeof(g_sMcastJoinReq));
	g_sMcastJoinReq.imr_multiaddr.s_addr = htonl(INADDR_ANY);
	g_sMcastJoinReq.imr_interface.s_addr = htonl(INADDR_ANY);
}

/**	@brief	print usage of the program
 */
void
print_usage(void)
{
	fprintf(stdout, " \
\n\
Usage: ipmc <-S|-R> <-g mcast_group_addr> [options] \n\
  Examples: \n\
	ipmc -S -g 224.1.1.100 \n\
	ipmc -S -g 224.1.1.100 -i 192.168.0.226 \n\
	ipmc -S -g 224.1.1.100 -b 1 \n\
	ipmc -R -g 224.1.1.100 \n\
	ipmc -R -g 224.1.1.100 -i 192.168.0.226 \n\
  Options are: \n\
	-h, --help \n\
		print help message. \n\
	-S, --sender \n\
		set Role to multicast sender. \n\
	-R, --receiver \n\
		set Role to multicast receiver. \n\
	-i, --mcast-local-if-addr 192.168.0.226 \n\
		Multicast Local Interface IP address. \n\
		Use this option to specify the local interface for \n\
		multicast traffic. As the default, the system will \n\
		select the proper interface. \n\
	-g, --mcast-group-addr 224.1.1.100 \n\
		Multicast Group IP address. \n\
		Range (224.0.0.1 - 239.255.255.255). \n\
	-p, --mcast-group-port 30333 \n\
		Multicast Group Target Port. UDP Port. \n\
		Range (1200 - 60000).\n\
	-d, --mcast-data data_string_to_send \n\
		Specify string to send as a multicast data. \n\
	-t, --mcast-ttl 1 \n\
		Multicast packet TTL (Time To Live). \n\
		Range (1 - 255) \n\
	-b, --mcast-loopback 0|1\n\
		Enable(1)/Disable(0) Multicast Loopback. \n\
\n\
	yuwan@innopiatech.com \n\
\n\
");
	fflush(stdout);
}

/**	Parse program options
 */
void
parse_options(int nArgc, char* ppArgv[])
{
	int nOpt = 0;

	while ( 1 ) {
#		ifdef USE_LONG_OPTIONS
		nOpt = getopt_long(nArgc, ppArgv,
			g_szOptions, g_arrLongOpts, NULL);
#		else /* USE_LONG_OPTIONS */
		nOpt = getopt(nArgc, ppArgv, g_szOptions);
#		endif /* USE_LONG_OPTIONS */

		if ( nOpt < 0 )
			break;	/* end of options */

		switch ( nOpt ) {
		case 'S':
			g_eRole = ROLE_SENDER;
			break;
		case 'R':
			g_eRole = ROLE_RECEIVER;
			break;
		case 'i':
			strcpy(g_szMcastLocalIfAddr, optarg);
			g_sMcastLocalIfAddr.s_addr = inet_addr(optarg);
			g_sMcastJoinReq.imr_interface.s_addr=inet_addr(optarg);
			break;
		case 'g':
			strcpy(g_szMcastGroupAddr, optarg);
			g_sMcastGroupAddr.sin_addr.s_addr = inet_addr(optarg);
			g_sMcastJoinReq.imr_multiaddr.s_addr=inet_addr(optarg);
			break;
		case 'p':
			g_nMcastGroupPort = atoi(optarg);
			g_sMcastGroupAddr.sin_port = htons(atoi(optarg));
			break;
		case 'd':
			strcpy(g_binData, optarg);
			break;
		case 't':
			g_nMcastTTL = atoi(optarg);
			break;
		case 'b':
			if ( atoi(optarg) )
				g_eLoopback = LOOPBACK_ENABLE;
			else
				g_eLoopback = LOOPBACK_DISABLE;
			break;
		case 'h':
		case '?':
		case ':':
			print_usage();
			exit(1);
		default:
			break;
		}
	}

	if ( g_eRole == ROLE_NONE ) {
		printf("%% You must specify one of -S or -R.\n");
		exit(1);
	}

	if ( strlen(g_szMcastGroupAddr) < 7 ) {
		printf("%% You must specify -g option for Mcast Group IP.\n");
		exit(1);
	}
}

/**	Create socket for send/receive multicast datagrams
 */
void
create_socket(void)
{
	g_nSocket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if ( g_nSocket < 0 ) {
		perror("Create socket failed.");
		exit(2);
	}
}

/**	Set Socket Option IP_MULTICAST_LOOP
 */
void
set_ip_multicast_loop(void)
{
	int nRet = 0;
	unsigned char g_nLoopback = 0;

	if ( g_eLoopback == LOOPBACK_ENABLE )
		g_nLoopback = 1;
	else if ( g_eLoopback == LOOPBACK_DISABLE )
		g_nLoopback = 0;
	else
		return;	/* no loopback requested */

	printf("Set IP_MULTICAST_LOOP %d.\n", g_nLoopback);

	nRet = setsockopt(g_nSocket, IPPROTO_IP, IP_MULTICAST_LOOP,
		&g_nLoopback, sizeof(g_nLoopback));

	if ( nRet < 0 ) {
		perror("setsockopt IP_MULTICAST_LOOP failed.");
		exit(3);
	}
}

/**	Set Socket Option IP_MULTICAST_TTL
 */
void
set_ip_multicast_ttl(void)
{
	int nRet = 0;

	printf("Set IP_MULTICAST_TTL value %d.\n", g_nMcastTTL);

	nRet = setsockopt(g_nSocket, IPPROTO_IP, IP_MULTICAST_TTL,
			(void*)&g_nMcastTTL, sizeof(g_nMcastTTL));

	if ( nRet < 0 ) {
		perror("setsockopt IP_MULTICAST_TTL failed.");
		exit(4);
	}
}

/**	Set Socket Option IP_MULTICAST_IF
 */
void
set_ip_multicast_if(void)
{
	int nRet = 0;

	printf("Set IP_MULTICAST_IF with %s.\n", g_szMcastLocalIfAddr);

	nRet = setsockopt(g_nSocket, IPPROTO_IP, IP_MULTICAST_IF,
		(const char*)&g_sMcastLocalIfAddr, sizeof(struct in_addr));

	if ( nRet < 0 ) {
		perror("setsockopt IP_MULTICAST_IF failed.");
		exit(5);
	}
}

/**	Set Socket Option IP_ADD_MEMBERSHIP
 */
void
set_ip_add_membership(void)
{
	int nRet = 0;

	printf("Set IP_ADD_MEMBERSHIP with (%s, %s).\n",
		g_szMcastLocalIfAddr, g_szMcastGroupAddr);

	nRet = setsockopt(g_nSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP,
		(void*)&g_sMcastJoinReq, sizeof(g_sMcastJoinReq));

	if ( nRet < 0 ) {
		perror("setsockopt IP_ADD_MEMBERSHIP failed.");
		exit(6);
	}
}

/**	Set Socket Option IP_DROP_MEMBERSHIP
 */
void
set_ip_drop_membership(void)
{
}

/**	Sender Role
 */
void
sender_role(void)
{
	int nRet = 0;
	int nLen = 0;
	int nCnt = 0;
	char binInfo[128];
	char binData[1024];

	create_socket();
	set_ip_multicast_ttl();
	set_ip_multicast_if();
	set_ip_multicast_loop();

	printf("Sending multicast data (%s -> %s:%d).\n",
		g_szMcastLocalIfAddr, g_szMcastGroupAddr, g_nMcastGroupPort);

	printf("Ctrl-C to stop.\n");

	while ( 1 ) {
		nCnt++;
		sprintf(binInfo, "[Cnt %05d, Src %s, Grp %s, Prt %d]",
			nCnt, g_szMcastLocalIfAddr, g_szMcastGroupAddr,
			g_nMcastGroupPort);
		sprintf(binData, "%s\n%s", binInfo, g_binData);
		nLen = strlen(binData);

		printf("	Sending...\n%s\n", binData);

		nRet = sendto(
				g_nSocket,	// socket desc
				binData,	// msg
				nLen,		// msg len
				0,		// flags (MSG_???)
				(struct sockaddr*)&g_sMcastGroupAddr,	// to
				sizeof(g_sMcastGroupAddr)	// to len
			);

		if ( nRet != nLen ) {
			perror("sendto failed.");
			exit(8);
		}

		sleep(1);
	}

	/* not reach */
}

/**	Bind socket to Multicast Group Address
 */
void
bind_mcast_socket(void)
{
	int nRet = 0;

	printf("Bind socket with multicast group %s.\n",
		g_szMcastGroupAddr);

retry_with_addr_any:
	nRet = bind(g_nSocket, (struct sockaddr*)&g_sMcastGroupAddr,
			sizeof(g_sMcastGroupAddr));

	if ( nRet < 0 ) {
		/* cygwin do not allow to bind with group address */
		if ( g_sMcastGroupAddr.sin_addr.s_addr != htonl(INADDR_ANY) )
		{
			g_sMcastGroupAddr.sin_addr.s_addr = htonl(INADDR_ANY);
			goto retry_with_addr_any;
		}

		perror("bind failed.");
		exit(9);
	}
}

/**	Receiver Role
 */
void
receiver_role(void)
{
	int nRet = 0;
	int nLen = 0;
	int nCnt = 0;
	int ii = 0;

	create_socket();
	bind_mcast_socket();
	set_ip_add_membership();

	printf("Receiving multicast data (%s -> %s:%d).\n",
		g_szMcastLocalIfAddr, g_szMcastGroupAddr, g_nMcastGroupPort);

	printf("Ctrl-C to stop.\n");

	while ( 1 ) {
		nLen = recvfrom(
				g_nSocket,	// socket desc
				g_binData,	// recv buffer
				sizeof(g_binData) - 1,	// buffer length
				0,		// flags (MSG_???)
				NULL,		// from
				0		// from len
			);

		if ( nLen < 0 ) {
			perror("recvfrom failed.");
			exit(10);
		}

		g_binData[nLen] = '\0';	// set end of string

		for ( ii=0; ii<nLen; ii++ ) {
			if ( g_binData[ii] == '\n' )
				continue;
			if ( g_binData[ii] == '\t' )
				continue;
			if ( g_binData[ii] == '\r' )
				continue;

			if ( g_binData[ii] >= ' ' && g_binData[ii] <= '~' )
				continue;

			g_binData[ii] = '.';	// mark un-printables
		}

		// printf("[Cnt%03d,Len%03d] %s\n", nCnt++, nLen, g_binData);
		printf("	Receiving...\n%s\n", g_binData);
	}

	/* not reach */
}

/**	@brief	main routine
 */
int
main(int nArgc, char* ppArgv[])
{
	init_global_variables();
	parse_options(nArgc, ppArgv);

	switch ( g_eRole ) {
	case ROLE_SENDER:
		sender_role();
		break;
	case ROLE_RECEIVER:
		receiver_role();
		break;
	}

	return 0;
}

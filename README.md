# ipmc : IP Multicast forwarding path testing tool

## the story
This tool is first written in 2004 during a project called OLT.  
And I'm keep using this tool until now.  
I brought this tool every company I worked for ever since,  
so some varients exist in those companies.  

But still the basic things are same, like:
* Sender/Receiver role
* Default port 30333

I'm putting this code here, in a hope people also enjoy the easy verification of multicast forwarding.

## build
I'd put a small Makefile, so you just need to type make, like below:

```
$ make
cc     ipmc.c   -o ipmc
```

It has many warnings, as it is written in the history.  
But will generate the binary.

## help
```
$ ./ipmc -?
./ipmc: invalid option -- '?'
 
Usage: ipmc <-S|-R> <-g mcast_group_addr> [options] 
  Examples: 
	ipmc -S -g 224.1.1.100 
	ipmc -S -g 224.1.1.100 -i 192.168.0.226 
	ipmc -S -g 224.1.1.100 -b 1 
	ipmc -R -g 224.1.1.100 
	ipmc -R -g 224.1.1.100 -i 192.168.0.226 
  Options are: 
	-h, --help 
		print help message. 
	-S, --sender 
		set Role to multicast sender. 
	-R, --receiver 
		set Role to multicast receiver. 
	-i, --mcast-local-if-addr 192.168.0.226 
		Multicast Local Interface IP address. 
		Use this option to specify the local interface for 
		multicast traffic. As the default, the system will 
		select the proper interface. 
	-g, --mcast-group-addr 224.1.1.100 
		Multicast Group IP address. 
		Range (224.0.0.1 - 239.255.255.255). 
	-p, --mcast-group-port 30333 
		Multicast Group Target Port. UDP Port. 
		Range (1200 - 60000).
	-d, --mcast-data data_string_to_send 
		Specify string to send as a multicast data. 
	-t, --mcast-ttl 1 
		Multicast packet TTL (Time To Live). 
		Range (1 - 255) 
	-b, --mcast-loopback 0|1
		Enable(1)/Disable(0) Multicast Loopback. 

	yuwan@innopiatech.com 

```

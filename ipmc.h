/**	@file	ipmc.h
 *	@brief	IP Multicast Test Program
 *
 *	@author	yuwan@innopiatech.com
 *	@date	2004. 12. 16.
 */

#ifndef _IPMC_H
#define _IPMC_H

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define USE_LONG_OPTIONS
#define	DEFAULT_UDP_TARGET_PORT	30333	//!< default mcast udp target port
#define	DEFAULT_MCAST_DATA	\
	"Sent from ipmc mcast program. yuwan@innopiatech.com"

typedef enum {
	ROLE_NONE = 0,
	ROLE_SENDER,
	ROLE_RECEIVER,
} Role;

typedef enum {
	LOOPBACK_NONE = 0,
	LOOPBACK_ENABLE,
	LOOPBACK_DISABLE,
} MCAST_LOOPBACK;

#ifndef _IPMC_C
#endif /* _IPMC_C */

#endif /* _IPMC_H */
